# Advanced Git Workshop
Lab 09: Finding bugs with Git Bisect

---

# Tasks

 - Searching contents within a repo

---

## Preparations

 - Let's clone a repository to work with:
 
```
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/static-web-app.git lab9
$ cd lab9
```

---

## Understanding the repository history

 - Check the repository history using
 
```
$ git log --oneline -n 30
```
```
bf19d7e (HEAD -> master, origin/master, origin/HEAD) update page title
770ab72 update nav title
d08de8e style improvements
d5a9a01 update bug
dc49be7 update footer
958d97c update main icon
92fc818 update contact section
30b017d update info section
b8a66cd update pictures section
5f372c5 introduce bug
fd9b95a update main title description
04ea586 update main title
8d01948 update nav item
5eeb955 update nav item
17cc553 update nav title
5ac1c83 update page title
960fede init
```

 - We already know the problematic commit, but let's see how git find it by himself
```
5f372c5 introduce bug
```
 
 - We also know that the **first commit is good**, and the **last commit is bad**:
```
960fede - Good Commit
bf19d7e - Bad Commit
```

---

## Finding the bug (manually)

 - Initialize git bisect using:
```
$ git bisect start
```

 - Set the last known good commit:
```
$ git bisect good 960fede
```

 - Set the first known bad commit:
```
$ git bisect bad bf19d7e
```

 - Bisect started, check the checked-out version status (open the index.html):

&nbsp;
<img alt="Image 9.1" src="Images/9.1.png"  width="700" border="1">
&nbsp;

 - Let know git the commit status (b8a66cd):
```
$ git bisect bad
```

 - Check the checked-out version status (open the index.html):

&nbsp;
<img alt="Image 9.2" src="Images/9.2.png"  width="700" border="1">
&nbsp;

 - Let know git the commit status (8d01948):
```
$ git bisect good
```

 - Check the checked-out version status (open the index.html):

&nbsp;
<img alt="Image 9.3" src="Images/9.3.png"  width="700" border="1">
&nbsp;

 - Let know git the commit status (fd9b95a):
```
$ git bisect good
```

 - Check the checked-out version status (open the index.html):

&nbsp;
<img alt="Image 9.4" src="Images/9.4.png"  width="700" border="1">
&nbsp;

 - Let know git the commit status (5f372c5):
```
$ git bisect bad
```

 - That's it, Git found the commit that introduced the bug:
```
5f372c51771e951b7293ed3f3961dcfc299be8a3 is the first bad commit
commit 5f372c51771e951b7293ed3f3961dcfc299be8a3
Author: Daniela <daniela@sela.co.il>
Date:   Wed Nov 14 10:59:42 2018 +0200

    introduce bug

:100644 100644 ff20f6f5f4749a92ae555863aafa4d2da2bc8205 43c245c5f1fa325dd400ca20d68b30e8e66e7802 M      index.html
```

 - Exit from the bisect mode:
```
$ git bisect reset
```

## Finding the bug (automatic)

 - To automate the process we can use the following command to know if a commit is good or not:
```
# check if the file index.html contain the word BUG (exit 1)
if grep -q "BUG" "index.html"; then exit 1; fi
```

 - Start bisect with the same parameters:
```
$ git bisect start
$ git bisect good 960fede
$ git bisect bad bf19d7e
```

 - Then configure git bisect to use a script to determine the commit status:
```
$ git bisect run sh -c '(if grep -q "BUG" "index.html"; then exit 1; fi)'
```

 - That's it, Git found the commit that introduced the bug:
```
5f372c51771e951b7293ed3f3961dcfc299be8a3 is the first bad commit
commit 5f372c51771e951b7293ed3f3961dcfc299be8a3
Author: Daniela <daniela@sela.co.il>
Date:   Wed Nov 14 10:59:42 2018 +0200

    introduce bug

:100644 100644 ff20f6f5f4749a92ae555863aafa4d2da2bc8205 43c245c5f1fa325dd400ca20d68b30e8e66e7802 M      index.html
```

 - Exit from the bisect mode:
```
$ git bisect reset
```
